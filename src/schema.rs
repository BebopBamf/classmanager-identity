table! {
    users (id) {
        id -> Uuid,
        role -> Varchar,
        username -> Varchar,
        email -> Varchar,
        password_hash -> Varchar,
    }
}
