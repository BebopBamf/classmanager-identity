use crate::DbPool;
use crate::models::management_models::NewUserData;
use crate::models::management_handlers::insert_user;

use actix_web::{web, post, Result, HttpResponse};

#[post("/user")]
pub async fn create_user(pool: web::Data<DbPool>, data: web::Json<NewUserData>) -> Result<HttpResponse> {
    let conn = pool.get().expect("couldn't get db connection from pool");
    
    let new_user = web::block(move || insert_user(&data, &conn))
        .await
        .map_err(|e| {
            eprintln!("{}", e);
            HttpResponse::InternalServerError().finish()
        })?;

    Ok(HttpResponse::Ok().json(new_user))
}
