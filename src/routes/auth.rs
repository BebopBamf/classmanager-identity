use crate::DbPool;
use crate::models::auth_handlers::select_user_from_username;

use actix_web::{web, post, Result, HttpResponse};
use jsonwebtoken::{encode, Header, Algorithm, EncodingKey};
use chrono::prelude::*;
use serde::{Serialize, Deserialize};
use bcrypt::verify;
use uuid::Uuid;

#[derive(Clone, Debug, Deserialize)]
pub struct LoginData {
    username: String,
    password: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct Claims {
    aud: String,
    exp: i64,
    iat: i64,
    iss: String,
    uuid: Uuid,
    username: String,
    email: String,
    role: String,
}

#[post("/login")]
pub async fn login(pool: web::Data<DbPool>, data: web::Json<LoginData>) -> Result<HttpResponse> {
    let conn = pool.get().expect("couldn't get db connection from pool");
    let password = data.password.clone();

    let user = web::block(move || select_user_from_username(&data.username, &conn))
        .await
        .map_err(|e| {
            eprintln!("{}", e);
            HttpResponse::InternalServerError().finish()
        })?;

    if !verify(password, &user.password_hash).unwrap() {
        return Ok(HttpResponse::Unauthorized().finish())
    }

    let claims = Claims { 
        aud: String::from("classmanager"),
        exp: Utc::now().timestamp() + 43200,
        iat: Utc::now().timestamp(),
        iss: String::from("classmanager-identity"),
        uuid: user.id,
        username: user.username,
        email: user.email,
        role: user.role,
    };
    
    let token = encode(&Header::new(Algorithm::RS256), &claims, &EncodingKey::from_rsa_pem(include_bytes!("private.pem")).unwrap()).unwrap();
    Ok(HttpResponse::Ok().json(token))
}
