use actix_web::web;

mod auth;
mod management;
mod students;

pub fn auth_config(cfg: &mut web::ServiceConfig) {
    cfg.service(auth::login);
}
