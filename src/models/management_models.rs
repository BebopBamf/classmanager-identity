use crate::schema::users;

use uuid::Uuid;
use serde::{Serialize, Deserialize};
use bcrypt::{DEFAULT_COST, hash};

#[derive(Clone, Debug, Serialize, Queryable)]
pub struct User {
    pub id: Uuid,
    pub role: String,
    pub username: String,
    pub email: String,
    pub password_hash: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct UserData {
    pub id: Uuid,
    pub role: String,
    pub username: String,
    pub email: String,
}

impl From<User> for UserData {
    fn from(user: User) -> Self {
        UserData {
            id: user.id,
            role: user.role,
            username: user.username,
            email: user.email,
        }
    }
}

#[derive(Clone, Debug, Insertable)]
#[table_name = "users"]
pub struct NewUser {
    pub id: Uuid,
    pub role: String,
    pub username: String,
    pub email: String,
    pub password_hash: String,
}

impl NewUser {
    pub fn from_new_user_data(new_user_data: &NewUserData) -> Self {
        let hash = hash(new_user_data.password.clone(), DEFAULT_COST).unwrap();

        NewUser {
            id: Uuid::new_v4(),
            role: new_user_data.role.clone(),
            username: new_user_data.username.clone(),
            email: new_user_data.email.clone(),
            password_hash: hash,
            
        }
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct NewUserData {
    pub role: String,
    pub username: String,
    pub email: String,
    pub password: String,
}

