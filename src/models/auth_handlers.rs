use crate::models::management_models::User;

use diesel::prelude::*;

pub fn select_user_from_username(user_name: &str, conn: &PgConnection) -> Result<User, diesel::result::Error> {
    use crate::schema::users::dsl::*;

    let user = users
        .filter(username.eq(user_name))
        .first::<User>(conn)?;

    Ok(user)
}
