use crate::models::management_models::*;
use crate::PgConnection;
use crate::schema::users::dsl::*;

use diesel::prelude::*;
use uuid::Uuid;

pub fn insert_user(new_user_data: &NewUserData, conn: &PgConnection) -> Result<UserData, diesel::result::Error> {
    let new_user = NewUser::from_new_user_data(new_user_data);
    
    let user: User = diesel::insert_into(users)
        .values(new_user)
        .get_result(conn)?;

    Ok(user.into())
}



pub fn select_user(user_id: &str, conn: &PgConnection) -> Result<UserData, diesel::result::Error> {
    let user = users
        .filter(id.eq(Uuid::parse_str(user_id).unwrap()))
        .first::<User>(conn)?;
    
    Ok(user.into())
}

pub fn select_users_by_role(user_role: &str, conn: &PgConnection) -> Result<Vec<UserData>, diesel::result::Error> {
    let results = users
        .filter(role.eq(user_role))
        .load::<User>(conn)?;
    
    let users_data: Vec<UserData> = results.into_iter().map(|user| {
        UserData::from(user)
    }).collect();

    Ok(users_data)
}

pub fn select_users(conn: &PgConnection) -> Result<Vec<UserData>, diesel::result::Error> {
    let results = users
        .load::<User>(conn)?;
    
    let users_data: Vec<UserData> = results.into_iter().map(|user| {
        UserData::from(user)
    }).collect();

    Ok(users_data)
}

pub fn update_user() {}

pub fn delete_user() {}
